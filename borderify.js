void (async function runSafe({
	logger = console,
	debug = () => {},
	croak = (e, log = true) => {
		log && logger.error(e)
		throw e;
	},
	...scope
}) {
	try {
		await init(...arguments);
	} catch (e) {
		logger.error(e)
		debug()
	}
	return;

	async function init() {
		logger.debug(this)
		attachListenerOnElementFocus(document.body)
		addShadow()
	}

	function attachListenerOnElementFocus(elm) {
		const wrapper = getWrapper()
		elm.addEventListener("focusin", appendWrapper(wrapper))
		elm.addEventListener("focusout", removeWrapper(wrapper))
	}



	function appendWrapper(wrapper){
		return function(evt){
			const target = evt.target
			target.matches(".Am[contenteditable]") || croak(0, false)
			scope.target = target
			attachElms(target.parentElement, wrapper);
		}
	}
	function removeWrapper(wrapper){
		return  function(evt){
			//wrapper.remove()
			//logger.debug('removed')
		}
	}

	function attachElms(target, elm) {
		target.append(elm);
	}

	function getWrapper() {
		document.querySelectorAll('#my-super-wrapper').forEach(x => {
			x.remove()
			logger.debug("getWrapper removed", x)
		})
		const elm = document.createRange().createContextualFragment(`
			<button id="my-super-wrapper">
				YEAH!
			</button>
		`).children[0]
		elm.addEventListener('click', actionTriggered())
		return elm
	}


	function actionTriggered(){
		return function(evt) {
			logger.log("actionTriggered", scope, evt)
			scope.target.innerHTML = `
				contenuto aggiunto ${(scope.count = (scope.count<<0)+1)}<br>
				${scope.target.innerHTML}
			`
		}
	}

	function addShadow(){
		document.body.append(
			document.createRange().createContextualFragment(
				`
				<style id="my-super-style">
					#my-super-wrapper {
						position: absolute;
						top: 0;
						background: yellow;
						z-index: 999999999
					}
					* {
						box-shadow: 0px 0px 10px 0px rgba(50, 50, 0, 0.2);
					}
		
					.Am {
						box-shadow: 0px 0px 10px 4px rgba(100, 0, 0, 0.9);
					}
				</style>
				`
			)
		)
	}
})({
	logger: {
		log() {
			console.log(`
			@my-ext!
			@ LOG
			`,
			...arguments)
		},
		debug() {
			console.debug(`
			@my-ext!
			@ DEBUG
			`,
			...arguments)
		},
		error() {
			console.error(`
			@my-ext!
			@ ERROR
			`,
			...arguments)
		}
	},
	debug() {
		debugger
	}
})

